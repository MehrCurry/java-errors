package com.iteratec.error;

import java.util.Iterator; 
import java.util.ArrayList; 

public class ModificationError { 
    public static void main(String args[]) { 
  
        // Creating an object of ArrayList 
        ArrayList<String> arr = new ArrayList<String>(); 
  
        arr.add("One"); 
        arr.add("Two"); 
        arr.add("Three"); 
        arr.add("Four"); 
  
        // Printing the elements 
        System.out.println("ArrayList: "); 
        Iterator<String> iter = arr.iterator(); 
  
        while (iter.hasNext()) { 
  
            System.out.print(iter.next() + ", "); 
  
            // ConcurrentModificationException 
            // will be raised here as an element 
            // is added during iteration 
            System.out.println("\n\nTrying to add an element in between iteration\n"); 
            arr.add("Five"); 
        } 
    } 
}
